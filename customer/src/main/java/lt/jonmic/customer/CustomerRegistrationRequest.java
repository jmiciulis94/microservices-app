package lt.jonmic.customer;

public record CustomerRegistrationRequest(
        String firstName,
        String lastName,
        String email) {
}
