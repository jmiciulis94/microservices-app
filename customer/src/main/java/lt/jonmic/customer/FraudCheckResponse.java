package lt.jonmic.customer;

public record FraudCheckResponse(Boolean isFraudster) {
}