package lt.jonmic.fraud;

public record FraudCheckResponse(Boolean isFraudster) {
}
